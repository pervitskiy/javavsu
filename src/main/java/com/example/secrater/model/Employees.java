package com.example.secrater.model;

import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class Employees {
    private UUID employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private Integer salary;
    private Set<JobHistory> jobHistory;


}
