package com.example.secrater.model;

import lombok.Data;

import java.util.Date;

@Data
public class JobHistory {
    private Integer jobHistoryId;
    private Date startDate;
    private Date finishDate;
    private String eventName;
}
