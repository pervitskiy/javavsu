package com.example.secrater.model;

import lombok.Data;

import java.util.Set;

@Data
public class Departments {
    private Integer departmentId ;
    private String departmentName ;
    private Set<Employees> employees;
}
