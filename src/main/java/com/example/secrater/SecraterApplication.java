package com.example.secrater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SecraterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecraterApplication.class, args);
	}

}
